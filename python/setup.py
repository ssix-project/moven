# -*- coding: utf-8 -*-

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


try:
    with open('requirements.txt', 'r') as f:
        _install_requires = [line.rstrip('\n') for line in f]
except IOError:
    _install_requires = ['jip']  # TODO


setup(
    name="moven",
    version="0.3.0.dev0",
    url="https://bitbucket.org/ssix-project/moven",
    author="Sergio Fernández",
    author_email="sergio.fernandez@redlink.co",
    description="NLP models distribution relying on Maven infrastructure",
    packages=['moven'],
    platforms=['any'],
    install_requires=_install_requires,
    entry_points={
        'console_scripts': [
            'moven = moven.moven:main'
        ]
    },
    classifiers=['Development Status :: 4 - Beta',
                 'Intended Audience :: Developers',
                 'Topic :: Software Development :: Build Tools',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Java',
                 'Environment :: Console'],
    keywords='python nlp ml dl models maven ssix',
    use_2to3=True
)
