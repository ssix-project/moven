# Moven Python infrastructure

Moven Python infrastructure relying on [jip](https://pypi.python.org/pypi/jip) for models retrieval.

## Installation

This implementation relies on [jip](https://pypi.python.org/pypi/jip) to manage the Maven 
dependencies. Therefore you would need `jip` in your system. 

You can either rely on a release (recommended):

    pip install moven

Or install the development version:

    sudo python setup.py install


## Configuration

To avoid unnecessary network usage, it is recommended to have added your local Maven repo to
your configuration at `$HOME/.jip_config` (`$VIRTUAL_ENV/.jip_config` if you're using a
virtual environment):

    [repos:local]
    uri=/home/<USERNAME>/.m2/repository/
    type=local

    [repos:central]
    uri=http://repo1.maven.org/maven2/
    type=remote
