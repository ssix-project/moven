package io.redlink.ssix.moven.models;
 
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.*;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;

/**
 * Copy Models Maven Mojo
 *
 * @author Sergio Fernández
 */
@Mojo(name="copy-models", threadSafe=true)
public class CopyModelsMojo extends AbstractMojo {

    @Parameter(defaultValue="${basedir}/src/main/models")
    private File source;

    @Parameter(defaultValue="${project.build.outputDirectory}/META-INF/resources/models")
    private File destination;

    //private boolean recursive; //TODO

    @Parameter(defaultValue = "false")
    private boolean compressed;

    @Parameter(defaultValue = "1024")
    private int bufferSize;

    public void execute() throws MojoExecutionException {
        final Log log = getLog();

        if (!source.isDirectory() || !source.canRead()) {
            log.warn("skip non existing " + source.getAbsolutePath());
        } else if (source.list().length == 0) {
            log.warn("skip empty directory " + source.getAbsolutePath());
        } else {
            //FileUtils.deleteDirectory(destination);
            destination.mkdirs();
            if (compressed) {
                copyCompressedFiles();
            } else {
                copyDirectory();
            }
        }
    }

    private void copyCompressedFiles() throws MojoExecutionException {
        final Log log = getLog();
        final Collection<File> files = FileUtils.listFiles(source, TrueFileFilter.TRUE, FalseFileFilter.FALSE);
        for (File file: files) {
            copyCompressedFile(file);
        }
        log.info("copied " + this.destination.listFiles((FileFilter) FileFileFilter.FILE).length + " compressed models " +
                "from '" + this.source.getAbsolutePath() + "' " +
                "to '" + destination.getAbsolutePath());
    }

    private void copyCompressedFile(File source) throws MojoExecutionException {
        final Log log = getLog();
        final File destination = new File(this.destination, String.format("%s.gz", source.getName()));
        log.debug(String.format("Compressing&copying %s to %s", source.getAbsolutePath(), destination.getAbsolutePath()));
        final long start = System.currentTimeMillis();
        try {
            final FileInputStream fis = new FileInputStream(source);
            final FileOutputStream fos = new FileOutputStream(destination);
            final GZIPOutputStream gos = new GZIPOutputStream(fos);

            byte[] buffer = new byte[bufferSize];
            int len;
            while((len=fis.read(buffer)) != -1){
                gos.write(buffer, 0, len);
            }
            gos.close();
            fos.close();
            fis.close();

            final long elapsed = System.currentTimeMillis() - start;
            final long minsElapsed = TimeUnit.MILLISECONDS.toMinutes(elapsed);
            final long secsElapsed = TimeUnit.MILLISECONDS.toSeconds(elapsed) - TimeUnit.MINUTES.toSeconds(minsElapsed);
            log.info(String.format("Compressed and copied %s to %s in %02d:%02d",
                    source.getAbsolutePath(), destination.getAbsolutePath(),
                    minsElapsed, secsElapsed));
        } catch (IOException e) {
            final String message = String.format("Error compressing/copying %s: %s",
                                                    source.getAbsolutePath(), e.getMessage());
            log.error(message);
            throw new MojoExecutionException(message, e);
        }
    }

    private void copyDirectory() throws MojoExecutionException {
        final Log log = getLog();
        log.debug("Copying files from " + source.getAbsolutePath() + " as they are");
        try {
            FileUtils.copyDirectory(source, destination);
            //TODO: generate an index file under a well-known location
            log.info("copied " + this.destination.listFiles((FileFilter) FileFileFilter.FILE).length + " models " +
                    "from '" + this.source.getAbsolutePath() + "' " +
                    "to '" + destination.getAbsolutePath());
        } catch (IOException e) {
            final String message = String.format("Error copying models: %s", e.getMessage());
            log.error(message);
            throw new MojoExecutionException(message, e);
        }
    }

}
